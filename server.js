
//préparation des modules
const express = require('express')
const app = express()
const mysql = require('mysql');
const bodyParser = require('body-parser');
const mustacheExpress = require('mustache-express');

app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', __dirname + '/public');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

//récupération données html
app.get('/', function (req, res) {
  var sql = `
      SELECT 
      titre, 
      contenu, 
      FROM Post 
`;

  res.render('accueil', {
      'titre': 'Mon blog !',
      'articles': 'blablablablablabalbalablabla'
  });
});




app.get("/formulaire", function (req, res) {
  res.render(__dirname + '/public/formulaire_register.html')
});

//connexion BDD
let con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "ace_base_de_donnees"
});

con.connect(function (err) {
  if (err) throw err;
  console.log("Connecté aux bases !");
});

//page d'inscription / register
app.post('/register/new', function (req, res) {
  let pseudo = req.body.pseudo;
  let mdp = req.body.mdp;
  let email = req.body.email;

  console.log(pseudo);
  console.log(mdp);
  console.log(email);

  con.query(`INSERT INTO User(pseudo, mot_de_passe, email) VALUES ("${pseudo}","${mdp}","${email}");`);
  res.redirect('/');
});

////page new article 
app.post('/article/new', function (req, res) {
  let titre = req.body.titre;
  let contenu = req.body.contenu;
  let date = req.body.date;

  console.log(titre);
  console.log(contenu);
  console.log(date);
  

  con.query(`INSERT INTO Post(titre, date, contenu) VALUES ("${titre}", "${date}", "${contenu}");`);
  res.redirect('/');
});

// request.query('SELECT * from ....', function (err, recordset) {

//   if (err) console.log(err)
//   res.send(recordset);

// });


app.listen(3000, function () {
  console.log('Connecté sur le port 3000!')
})


-- MySQL Script generated by MySQL Workbench
-- jeu. 12 sept. 2019 17:01:41 CEST
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ace_base_de_donnees
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ace_base_de_donnees` ;

-- -----------------------------------------------------
-- Schema ace_base_de_donnees
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ace_base_de_donnees` DEFAULT CHARACTER SET utf8 ;
USE `ace_base_de_donnees` ;

-- -----------------------------------------------------
-- Table `ace_base_de_donnees`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ace_base_de_donnees`.`User` ;

CREATE TABLE IF NOT EXISTS `ace_base_de_donnees`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NULL,
  `pseudo` VARCHAR(45) NULL,
  `mot_de_passe` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ace_base_de_donnees`.`Post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ace_base_de_donnees`.`Post` ;

CREATE TABLE IF NOT EXISTS `ace_base_de_donnees`.`Post` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `titre` VARCHAR(45) NULL,
  `date` DATE NULL,
  `contenu` LONGTEXT NULL,
  `User_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Post_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_Post_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `ace_base_de_donnees`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ace_base_de_donnees`.`Comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ace_base_de_donnees`.`Comment` ;

CREATE TABLE IF NOT EXISTS `ace_base_de_donnees`.`Comment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NULL,
  `contenu` LONGTEXT NULL,
  `User_id` INT NOT NULL,
  `Post_id` INT NOT NULL,
  PRIMARY KEY (`id`, `User_id`),
  INDEX `fk_Comment_User1_idx` (`User_id` ASC),
  INDEX `fk_Comment_Post1_idx` (`Post_id` ASC),
  CONSTRAINT `fk_Comment_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `ace_base_de_donnees`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comment_Post1`
    FOREIGN KEY (`Post_id`)
    REFERENCES `ace_base_de_donnees`.`Post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
